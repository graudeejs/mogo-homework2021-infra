# Install
Install dependencies
```
ansible-galaxy install -r requirements.yml
```


# Deployment
```
ansible-playbook setup-bastion.yml -e target=bastions -u adminuser
ansible-playbook setup-support-server.yml -e target=support_servers -u adminuser
ansible-playbook setup-db-server.yml -e target=databases -u adminuser
ansible-playbook setup-app-server.yml -e target=apps -u adminuser
```


# Grafana dahboards to manually import:
* 1860
* 8378
