---
- become: yes
  tags:
    - postgresql
  block:
    - name: Ensure all configured locales are present.
      locale_gen:
        name: "{{ item }}"
        state: present
      with_items:
        - "en_US.UTF-8"

    - name: "install postgresql"
      package:
        name:
          - postgresql
          - postgresql-contrib
          - python-psycopg2 # ansible needs this (python2)
        state: present
      notify:
        - "restart postgresql"

    - name: Check if PostgreSQL database is initialized.
      stat:
        path: "/var/lib/postgresql/11/data/PG_VERSION"
      register: postgresql_data_dir_version

    - name: Ensure PostgreSQL database is initialized.
      shell: "su -m postgres -c '/usr/lib/postgresql/11/bin/initdb -D /var/lib/postgresql/11/data'"
      when: not postgresql_data_dir_version.stat.exists
      args:
        warn: false

    - name: "start/enable postgresql"
      service:
        name: postgresql
        state: started
        enabled: yes

    - name: "install configuration files"
      template:
        src: "templates/postgresql.conf.j2"
        dest: "/etc/postgresql/11/main/postgresql.conf"
        mode: og=
        owner: postgres
        group: postgres
      register: "postgresql_conf"

    - name: "install configuration files"
      template:
        src: "templates/{{ item }}.j2"
        dest: "/etc/postgresql/11/main/{{ item }}"
        mode: og=
        owner: postgres
        group: postgres
      with_items:
        - pg_hba.conf
        - pg_ident.conf
      register: "postgresql_config_files"

    - name: "restart postgresql"
      service:
        name: "postgresql"
        state: restarted
      when: "postgresql_conf.changed"

    - name: "reload postgresql"
      service:
        name: "postgresql"
        state: reloaded
      when:
        - "not postgresql_conf.changed"
        - "postgresql_config_files.changed"

    - name: "Create db owners"
      postgresql_user:
        name: "{{ item.owner }}"
        encrypted: true
        state: "{{ postgresql_users[item.owner].state | default('present') }}"
        groups: "{{ postgresql_users[item.owner].groups | default(omit) }}"
      with_items: "{{ postgresql_databases }}"

    - name: "create databases"
      postgresql_db:
        name: "{{ item.name }}"
        lc_collate: "en_US.UTF-8"
        lc_ctype: "en_US.UTF-8"
        encoding: "UTF-8"
        template: "{{ item.template | default('template0') }}"
        owner: "{{ item.owner | default('postgres') }}"
        state: "{{ item.state | default('present') }}"
      with_items: "{{ postgresql_databases }}"

    - name: "Create postgresql users"
      postgresql_user:
        name: "{{ item.key }}"
        encrypted: true
        state: "{{ item.value.state | default('present') }}"
        groups: "{{ item.value.groups | default(omit) }}"
      with_dict: "{{ postgresql_users }}"

    - name: "set postgresql passwords"
      postgresql_user:
        name: "{{ item.key }}"
        password: "md5{{ (vault_postgresql_passwords[item.key] + item.key) | hash('md5') }}"
      when:
        - "item.value.state | default('present') == 'present'"
        - "item.key in vault_postgresql_passwords"
      with_dict: "{{ postgresql_users }}"
      no_log: true
