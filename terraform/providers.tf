terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.46.0"
    }
  }

  backend "azurerm" {
    resource_group_name = "support-infra"
    storage_account_name = "aberjozaterraform"
    container_name = "terraform-state"
    key = "aberjoza-mogo-demo2020.tfstate"
  }
}

provider "azurerm" {
  features {
    virtual_machine {
      delete_os_disk_on_deletion = true
    }
  }
}
