module "support_subnet" {
  source = "./modules/subnets"
  subnet_role = "support"
  subnet_cidrs = ["10.0.1.0/24"]
  open_tcp_port_ranges = ["80", "443"]
  resource_group = azurerm_resource_group.eu_west
  virtual_network_name = azurerm_virtual_network.default.name
  providers = {
    azurerm = azurerm
  }
}

module "support_server" {
  source = "./modules/server"
  resource_group = azurerm_resource_group.eu_west
  server_name = "support"
  subnet_id = module.support_subnet.subnet_id
  app_sec_group_id = module.support_subnet.app_security_group_id
  private_ip = "10.0.1.4"
  create_public_ip = true
  vm_size = "Standard_B1s"
}
