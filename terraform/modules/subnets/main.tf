variable "subnet_role" {
  type = string
}
variable "subnet_cidrs" {
  type = list(string)
}
variable "resource_group" {}
variable "virtual_network_name" {
  type = string
}
variable "open_tcp_port_ranges" {
  type = list(string)
  default = []
}
variable "open_udp_port_ranges" {
  type = list(string)
  default = []
}

resource "azurerm_subnet" "default" {
  name                 = "${var.resource_group.name}-${var.subnet_role}"
  resource_group_name  = var.resource_group.name
  virtual_network_name = var.virtual_network_name
  address_prefixes     = var.subnet_cidrs
}

resource "azurerm_application_security_group" "default" {
  name = "${var.resource_group.name}-${var.subnet_role}"
  resource_group_name = var.resource_group.name
  location            = var.resource_group.location
}

resource "azurerm_network_security_group" "default" {
  name                = "net_${var.subnet_role}_security_group"
  location            = var.resource_group.location
  resource_group_name = var.resource_group.name
}

resource "azurerm_subnet_network_security_group_association" "default" {
  subnet_id                 = azurerm_subnet.default.id
  network_security_group_id = azurerm_network_security_group.default.id
}

resource "azurerm_network_security_rule" "tcp_in" {
  count = length(var.open_tcp_port_ranges)
  name                        = "net_${var.subnet_role}_security_group_tcp_in${count.index}"
  priority                    = 300 + count.index
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = var.open_tcp_port_ranges[count.index]
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name = var.resource_group.name
  network_security_group_name = azurerm_network_security_group.default.name
}

resource "azurerm_network_security_rule" "udp_in" {
  count = length(var.open_udp_port_ranges)
  name                        = "net_${var.subnet_role}_security_group_udp_in${count.index}"
  priority                    = 400 + count.index
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Udp"
  source_port_range           = "*"
  destination_port_range      = var.open_udp_port_ranges[count.index]
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name = var.resource_group.name
  network_security_group_name = azurerm_network_security_group.default.name
}

resource "azurerm_network_security_rule" "allow_tcp_out" {
  name                        = "net_${var.subnet_role}_security_group_tcp_out_all"
  priority                    = 200
  direction                   = "Outbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "*"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name = var.resource_group.name
  network_security_group_name = azurerm_network_security_group.default.name
}

resource "azurerm_network_security_rule" "allow_udp_out" {
  name                        = "net_${var.subnet_role}_security_group_udp_out_all"
  priority                    = 201
  direction                   = "Outbound"
  access                      = "Allow"
  protocol                    = "Udp"
  source_port_range           = "*"
  destination_port_range      = "*"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name = var.resource_group.name
  network_security_group_name = azurerm_network_security_group.default.name
}

output "group_id" {
  value = azurerm_network_security_group.default.id
}

output "subnet_id" {
  value = azurerm_subnet.default.id
}

output "app_security_group_id" {
  value = azurerm_application_security_group.default.id
}
