variable "resource_group" {}
variable "server_name" {
  type = string
}
variable "subnet_id" {
  type = string
}
variable "private_ip" {
  type = string
}
variable "app_sec_group_id" {
  type = string
}
variable "vm_size" {
  type = string
  default = "Standard_B1ls"
}
variable "create_public_ip" {
  type = bool
  default = false
}
variable "identity" {
  type = list
  default = []
}


resource "azurerm_public_ip" "default" {
  count               = var.create_public_ip ? 1 : 0
  name                = "${var.resource_group.name}-${var.server_name}-pub-ip"
  resource_group_name = var.resource_group.name
  location            = var.resource_group.location
  allocation_method   = "Static"
  ip_version = "IPv4"
}


resource "azurerm_network_interface" "default" {
  name                = "${var.resource_group.name}-${var.server_name}-primary-nic"
  resource_group_name = var.resource_group.name
  location            = var.resource_group.location

  ip_configuration {
    name                          = "internal"
    subnet_id                     = var.subnet_id
    private_ip_address_allocation = "Static"
    private_ip_address = var.private_ip
    public_ip_address_id = var.create_public_ip ? azurerm_public_ip.default[0].id : ""
  }
}

resource "azurerm_network_interface_application_security_group_association" "default" {
  network_interface_id          = azurerm_network_interface.default.id
  application_security_group_id = var.app_sec_group_id
}

resource "azurerm_linux_virtual_machine" "default" {
  name                = "${var.resource_group.name}-${var.server_name}"
  resource_group_name = var.resource_group.name
  location            = var.resource_group.location
  size                = var.vm_size
  admin_username      = "adminuser"
  network_interface_ids = [
    azurerm_network_interface.default.id
  ]

  admin_ssh_key {
    username   = "adminuser"
    public_key = file("files/aberjoza.pub")
  }

  os_disk {
    name                 = "${var.resource_group.name}-${var.server_name}-primary-disk"
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Debian"
    offer     = "debian-10"
    sku       = "10-backports-gen2"
    version   = "latest"
  }

  dynamic "identity" {
    for_each = var.identity
    content {
      type = identity.value["type"]
    }
  }
}

output "virtual_machine_id" {
  value = azurerm_linux_virtual_machine.default.virtual_machine_id
}
output "id" {
  value = azurerm_linux_virtual_machine.default.id
}
