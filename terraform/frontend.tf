module "frontend_subnet" {
  source = "./modules/subnets"
  subnet_role = "frontend"
  subnet_cidrs = ["10.0.3.0/24"]
  open_tcp_port_ranges = ["80", "443"]
  resource_group = azurerm_resource_group.eu_west
  virtual_network_name = azurerm_virtual_network.default.name
  providers = {
    azurerm = azurerm
  }
}

module "web_server" {
  source = "./modules/server"
  resource_group = azurerm_resource_group.eu_west
  server_name = "web"
  subnet_id = module.frontend_subnet.subnet_id
  app_sec_group_id = module.frontend_subnet.app_security_group_id
  private_ip = "10.0.3.4"
  create_public_ip = true
  vm_size = "Standard_B1s"
}
