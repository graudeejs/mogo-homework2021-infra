module "bastion_subnet" {
  source = "./modules/subnets"
  subnet_role = "bastion"
  subnet_cidrs = ["10.0.0.0/24"]
  open_tcp_port_ranges = ["22"]
  resource_group = azurerm_resource_group.eu_west
  virtual_network_name = azurerm_virtual_network.default.name
  providers = {
    azurerm = azurerm
  }
}

module "bastion_server" {
  source = "./modules/server"
  resource_group = azurerm_resource_group.eu_west
  server_name = "bastion"
  subnet_id = module.bastion_subnet.subnet_id
  app_sec_group_id = module.bastion_subnet.app_security_group_id
  private_ip = "10.0.0.4"
  create_public_ip = true
}
