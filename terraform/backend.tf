module "backend_subnet" {
  source = "./modules/subnets"
  subnet_role = "backend"
  subnet_cidrs = ["10.0.2.0/24"]
  resource_group = azurerm_resource_group.eu_west
  virtual_network_name = azurerm_virtual_network.default.name
  providers = {
    azurerm = azurerm
  }
}

module "db_server" {
  source = "./modules/server"
  resource_group = azurerm_resource_group.eu_west
  server_name = "db"
  subnet_id = module.backend_subnet.subnet_id
  app_sec_group_id = module.backend_subnet.app_security_group_id
  private_ip = "10.0.2.4"
  create_public_ip = false
  vm_size = "Standard_B1s"
}

