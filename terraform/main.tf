data "azurerm_subscription" "default" {
}

data "azurerm_client_config" "default" {
}

resource "azurerm_resource_group" "eu_west" {
  name     = "eu-west"
  location = "West Europe"
}

resource "azurerm_virtual_network" "default" {
  name                = "default-network"
  resource_group_name = azurerm_resource_group.eu_west.name
  location            = azurerm_resource_group.eu_west.location
  address_space       = ["10.0.0.0/16"]
}
