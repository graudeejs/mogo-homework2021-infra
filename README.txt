This homework took way longer than I expected.

I decided to make some things different than requested in homework task:

1) Since Mogo is fintech company, security should be top priority.
  For this reason I created bastion server (jumphost). SSH access to any
  other server can be only done though this server.

  I also added different subnets for different server roles.  Currently within
  10.0.0.0/16 subnet all server can communicate to each other.  I wanted to
  limit access between /24 subnets to only required ports using Network and
  Application Security Groups. However as time was running, I moved forward and
  didn't implement this feature. After all it was my first day with Azure.

2) I didn't use rvm, because ansible galaxy role failed and I didn't want to
  investigate that (possibly enabling pipelineing in ansible.cfg would have
  fixed that, but I moved forward).

  Instead of rvm I used asdf-vm (https://asdf-vm.com). It has advantage that it
  supports not only ruby, but also python, nodejs and many other languages and
  not only languages. This allowed me to setup ruby and nodejs using one tool.

3) I installed Prometheus/Grafana on dedicated server, mostly because I wanted
  to implement network policies (open only required ports between subnets).

4) I created SSL certificates for planned hosts under one of domain names that
  I own.

5) I created role for DB access and assigned that role to 3x users (app users,
  my user and demo user)

6) Added mailcatcher and configured alertmanager to send e-mail notificatiosn
  to it (didn't test as of writing this)


I also tried to configure prometheus autodiscovery, however I failed at that.
Need to learn more about how Azure works, thus I moved forward and implemented
static scrape configuration.


Naturally there are many things to improve:
* Explicitly setup servers to use UTC timezone
* Explicitly setup ntp configuration
* Change root password. Preferably to randomly generated one and store it in
  something like Hashicorp vault).
* I would implement better and more flexible system user management role
* For my production I wouldn't compile assets on production server. Instead I
  would create automatic pipeline that would compile assets and create some tar
  file containing static assets and ruby code, or perhaps separate assets from
  ruby code)
* If rails is to run on same server as nginx, then using unix socket would be
  much better, however I was already tired of doing this task, thus I left
  default (tcp socket).

Also note that my ansible roles are far from perfect (some might not define
default values). Again that's because this task took longer than expected.
